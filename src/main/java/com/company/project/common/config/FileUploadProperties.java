package com.company.project.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 文件上传参数配置类
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Component
@ConfigurationProperties(prefix = "file")
public class FileUploadProperties {

    private String path;
    private String rootDir;
    private String accessUrl;

    public void setRootDir(String rootDir) {
        this.rootDir = rootDir;
        //set accessUrl
        if (StringUtils.isEmpty(rootDir)) {
            this.accessUrl = null;
        }
        this.accessUrl = rootDir + "/**";
    }

    public String getAccessUrl() {
        return accessUrl;
    }

    public String getRootDir() {
        return rootDir;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }



}